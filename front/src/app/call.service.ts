import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Call } from './call'
import { Subject, Observable } from "rxjs"

import * as io from "socket.io-client"


@Injectable({
  providedIn: 'root'
})
export class CallService {

  private callStatus = new Subject<string>()
  private socket = io("http://localhost:3000")

  private apiUrl: string = 'http://localhost:3000'
  private callId = new Subject<number>()
  constructor(private http: HttpClient) {
    this.socket.on("message", status => {
      this.callStatus.next(status)
    })


  }

  getCallStatus(): Observable<string> {
    return this.callStatus.asObservable()
  }

  placeCall(number: string) {
    const postData = { number1: '999999999', number2: number }
    this.http.post<Call>(this.apiUrl + '/call', postData).subscribe(data => {
      this.callId.next(data.id)
    });
  }

  getCallId(): Observable<number> {
    return this.callId.asObservable()
  }


}
