import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {

  number: string
  validator = /(^[0-9]{9}$)/

  constructor(private callService: CallService) { }

  state: string = "waiting"
  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state
    })
  }


  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.callService.getCallId().subscribe(id => {
        this.checkStatus()
      })

    } else {
      console.info("Numer niepoprawny")
    }
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }



  ngOnInit(): void {
  }

}
