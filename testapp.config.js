module.exports = {
    apps : [{
    name: 'TestAPP', // nazwa aplikacji
    script: 'app.js', // skrypt uruchamiający aplikację
    instances: 1, // ilość instancji
    autorestart: true, // spróbuje ponownie uruchomić aplikację jeśli próba uruchomienia zakończyła się błędem
    watch: true, // automatycznie zrestartuje aplikację jak wykryje zmiany w plikach
    max_memory_restart: '1G', // autorestart jeśli aplikacja przekracza 1GB pamięci
    env: { // definicja środowiska domyślnego
    "PORT": 3000,
    "NODE_ENV": 'development'
    }, // definicja dodatkowego środowiska
    env_production: {
    "PORT": 2000,
    "NODE_ENV": 'production'
    },
    log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS', //format logów
    error_file: '/var/log/testapp.log', // plik gdzie mają być zapisywane błędy
    out_file: '/var/log/testapp.log' // plik gdzie mają być zapisywane logi
    }]
   };